use bevy::prelude::*;

mod visualization;
use crate::visualization::*;

mod story;
use crate::story::*;


fn main() {
  App::new()
    .add_plugins(DefaultPlugins)
    .add_plugin(VisualizationPlugin)
    .add_plugin(StoryPlugin)
    .run();
}
