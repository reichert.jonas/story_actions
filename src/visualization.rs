use bevy::prelude::*;
use bevy_inspector_egui::bevy_egui::{egui, EguiContext};
use bevy_inspector_egui::quick::WorldInspectorPlugin;

use crate::story::*;

pub struct VisualizationPlugin;

impl Plugin for VisualizationPlugin {
  fn build(&self, app: &mut App) {
    app.init_resource::<UiState>()
    .add_plugin(WorldInspectorPlugin)
    .add_startup_system(add_camera)

    .add_system(camera_controls)
    .add_system(build_ui);
  }
}

#[derive(Default, Resource)]
struct UiState {
    add_height_value: f32,
    edit_story_value: i32,
    edit_height_value: f32,
    swap1_story_value: i32,
    swap2_story_value: i32,
    delete_story_value: i32,
    underground: bool,
}

fn add_camera(
  mut commands: Commands,
) {
  commands.spawn(Camera3dBundle {
    transform: Transform::from_xyz(0.0, 3.0, 30.0).looking_at(Vec3::ZERO, Vec3::Y),
    ..default()
  });
}

fn camera_controls(
  keyboard: Res<Input<KeyCode>>,
  mut query: Query<&mut Transform, With<Camera3d>>,
  time: Res<Time>
) {
  let mut camera = query.single_mut();

  let mut forward = camera.forward();
  forward.y = 0.0;
  forward = forward.normalize();

  let mut left = camera.left();
  left.y = 0.0;
  left = left.normalize();

  let mut up = camera.up();
  up.x = 0.0;
  up = up.normalize();

  let speed = 6.0;
  let rotate_speed = 0.6;
  //Leafwing
  if keyboard.pressed(KeyCode::W) {
      camera.translation += forward * time.delta_seconds() * speed;
  }
  if keyboard.pressed(KeyCode::S) {
      camera.translation -= forward * time.delta_seconds() * speed;
  }
  if keyboard.pressed(KeyCode::A) {
      camera.translation += left * time.delta_seconds() * speed;
  }
  if keyboard.pressed(KeyCode::D) {
      camera.translation -= left * time.delta_seconds() * speed;
  }
  if keyboard.pressed(KeyCode::Space) {
    camera.translation += up * time.delta_seconds() * speed;
  }
  if keyboard.pressed(KeyCode::LAlt) {
      camera.translation -= up * time.delta_seconds() * speed;
  }
  if keyboard.pressed(KeyCode::Q) {
      camera.rotate_axis(Vec3::Y, rotate_speed * time.delta_seconds())
  }
  if keyboard.pressed(KeyCode::E) {
      camera.rotate_axis(Vec3::Y, -rotate_speed * time.delta_seconds())
  }
  if keyboard.pressed(KeyCode::F) {
    camera.rotate_axis(Vec3::X, rotate_speed * time.delta_seconds())
  }
  if keyboard.pressed(KeyCode::R) {
      camera.rotate_axis(Vec3::X, -rotate_speed * time.delta_seconds())
  }
}

fn build_ui(
  //asset_server: Res<AssetServer>,
  mut egui_context: ResMut<EguiContext>,
  mut ui_state: ResMut<UiState>,
  mut add_evw: EventWriter<AddStoryEvent>,
  mut edit_evw: EventWriter<EditStoryEvent>,
  mut swap_evw: EventWriter<SwapStoryEvent>,
  mut delete_evw: EventWriter<DeleteStoryEvent>,
  query: Query<&Level>,
) {
  let hs = query.iter().find(|x| x.0 == query.iter().max().unwrap().0).unwrap().0;
  let ls = query.iter().find(|x| x.0 == query.iter().min().unwrap().0).unwrap().0;

  egui::Window::new("Dev Tool").show(egui_context.ctx_mut(), |ui| {
    // Camera instructions
    ui.label("Camera instructions:");
    ui.label("W/S: Forward/Backward");
    ui.label("A/D: Left/Right");
    ui.label("Q/E: Rotate Right/Left");
    ui.label("Space/LAlt: Up/Down");

    ui.separator();
    
    // Add Story
    ui.label("Add");
    ui.add(egui::DragValue::new(&mut ui_state.add_height_value).prefix("Height: ").clamp_range(0.1..=1000.0));
    ui.checkbox(&mut ui_state.underground, "Underground");
    if ui.button("Add").clicked() {
      let mut new_story_type = StoryType::Overground;
      if ui_state.underground {
        new_story_type = StoryType::Underground;
      }

      add_evw.send(AddStoryEvent(ui_state.add_height_value, new_story_type))
    }

    ui.separator();

    // Edit Story
    ui.label("Edit");
    ui.add(egui::DragValue::new(&mut ui_state.edit_story_value).prefix("Story: ").clamp_range(ls..=hs));
    ui.add(egui::DragValue::new(&mut ui_state.edit_height_value).prefix("Height: ").clamp_range(0.1..=1000.0));
    if ui.button("Edit").clicked() {
      edit_evw.send(EditStoryEvent(ui_state.edit_story_value, ui_state.edit_height_value));
    }

    ui.separator();

    // Swap Stories
    ui.label("Swap");
    ui.add(egui::DragValue::new(&mut ui_state.swap1_story_value).prefix("Story 1: ").clamp_range(ls..=hs));
    ui.add(egui::DragValue::new(&mut ui_state.swap2_story_value).prefix("Story 2: ").clamp_range(ls..=hs));
    if ui.button("Swap").clicked() {
      swap_evw.send(SwapStoryEvent(ui_state.swap1_story_value, ui_state.swap2_story_value));
    }

    ui.separator();

    // Delete Story
    ui.label("Delete");
    ui.add(egui::DragValue::new(&mut ui_state.delete_story_value).prefix("Story: ").clamp_range(ls..=hs));
    if ui.button("Delete").clicked() {
      delete_evw.send(DeleteStoryEvent(ui_state.delete_story_value));
    }
  });
}
