use bevy::prelude::*;

pub struct StoryPlugin;

impl Plugin for StoryPlugin {
  fn build(&self, app: &mut App) {
    app
    .add_event::<AddStoryEvent>()
    .add_event::<EditStoryEvent>()
    .add_event::<SwapStoryEvent>()
    .add_event::<DeleteStoryEvent>()
    
    .add_startup_system(add_ground_floor)

    .add_system_to_stage(CoreStage::PreUpdate, add_story)
    .add_system_to_stage(CoreStage::PreUpdate, edit_story)
    .add_system_to_stage(CoreStage::PreUpdate, swap_story)
    .add_system_to_stage(CoreStage::PreUpdate, delete_story)

    .add_system(rearrange_stories)
    .run();
  }
}

pub enum StoryType{
  Overground,
  Underground
}

#[derive(Component, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct Level(pub i32);

#[derive(Component, Clone)]
pub struct Height(pub f32);

// Takes the height of the new story and it's type
pub struct AddStoryEvent(pub f32, pub StoryType);
// Takes the Level number and the new height of the story
pub struct EditStoryEvent(pub i32, pub f32);
// Takes the level of the stories to swap
pub struct SwapStoryEvent(pub i32, pub i32);
// Takes the Level number of the story which should be deleted
pub struct DeleteStoryEvent(pub i32);

fn add_ground_floor(
  mut commands: Commands,
  mut meshes: ResMut<Assets<Mesh>>,
  mut materials: ResMut<Assets<StandardMaterial>>,
) {
  commands.spawn((Level(0), Height(5.0)))
  .insert(PbrBundle {
    mesh: meshes.add(Mesh::from(shape::Box {min_x: -2.5, max_x: 2.5, min_y: -2.5, max_y: 2.5, min_z: -2.5, max_z: 2.5})),
    material: materials.add(Color::rgb(255.0, 255.0, 255.).into()),
    ..default()
  });
}

fn add_story(
  mut commands: Commands,
  mut evr: EventReader<AddStoryEvent>,
  mut meshes: ResMut<Assets<Mesh>>,
  mut materials: ResMut<Assets<StandardMaterial>>,
  query: Query<(&Level, &Height, &Transform)>,
) {
  if evr.iter().len() <= 0 {
    return;
  }

  // Gets the highest and lowest story to determine the level of the new story
  let mut highest_story = query.iter().find(| x| x.0.0 == query.iter().map(|(l,_,_)|l).max().unwrap().0).unwrap().0.0;
  let mut lowest_story = query.iter().find(| x| x.0.0 == query.iter().map(|(l,_,_)|l).min().unwrap().0).unwrap().0.0;  

  for ev in evr.iter() {
    // Height needs to be positive
    if ev.0 <= 0.0 {
      continue;
    }

    commands.spawn((
      // assigns the new level and in/decrements the highest/lowest_story
      match ev.1 {
        StoryType::Overground => {
          highest_story += 1;
          Level(highest_story)
        }
        StoryType::Underground => {
          lowest_story -= 1;
          Level(lowest_story)
        }
      },
      Height(ev.0)))
      .insert(PbrBundle {
        mesh: meshes.add(Mesh::from(shape::Box {min_x: -2.5, max_x: 2.5, min_y: -ev.0 / 2.0, max_y: ev.0 / 2.0, min_z: -2.5, max_z: 2.5})),
        material: materials.add(Color::rgb(0.67, 0.84, 0.92).into()),
        ..default()
      });
  }
}

fn edit_story(
  mut commands: Commands,
  mut meshes: ResMut<Assets<Mesh>>,
  mut materials: ResMut<Assets<StandardMaterial>>,
  mut evr: EventReader<EditStoryEvent>,
  mut query: Query<(Entity, &Level, &mut Height)>,
) {
  if evr.iter().len() <= 0 {
    return;
  }

  for ev in evr.iter() {
    // Height needs to be positive
    if ev.1 <= 0.0 {
      continue;
    }

    // Check if story exists
    match query.iter_mut().find(|x| x.1.0 == ev.0) {
      Some(mut story) => {
        story.2.0  = ev.1;

        // Set the color of the cube (white for ground story, else grey)
        let mut material: StandardMaterial = Color::rgb(0.67, 0.84, 0.92).into();
        if ev.0 == 0 {
          material = Color::rgb(255., 255., 255.).into();
        }
    
        commands.entity(story.0).insert(
          PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Box {min_x: -2.5, max_x: 2.5, min_y: -ev.1 / 2.0, max_y: ev.1 / 2.0, min_z: -2.5, max_z: 2.5})),
            material: materials.add(material),
            ..default()
          });
      }
      None => {
        println!("Story not found!");
        continue
      }
    }
  }
}

fn swap_story(
  mut evr: EventReader<SwapStoryEvent>,
  mut query: Query<(Entity, &mut Level, &Height)>,
) {
  if evr.iter().len() <= 0 {
    return;
  }

  // Check if story is 0 or if the story is swapped with itself
  for ev in evr.iter() {
    if ev.0 == 0 ||
     ev.1 == 0 ||
     ev.0 == ev.1 {
      continue;
    }

    // Check if the stories exists
    if query.iter().find(|x| x.1.0 == ev.0).is_none() ||
      query.iter().find(|x| x.1.0 == ev.1).is_none() {
      println!("At least one of the stories does not exist!");
      continue;
    }

    // Variable used to prevent to get the swapped story
    let mut pre_story_id: u32 = 0;
    
    for i in 0..2 {
      match i {
        0 => {
          // Get first story to swap and change the level of it
          let mut story = query.iter_mut().find(|x| x.1.0 == ev.0).unwrap();
          story.1.0 = ev.1;
          pre_story_id = story.0.index();
          }
        1 => {
          // Get second story to swap and check that it's not the first one and change level of it
          let mut story =  query.iter_mut().find(|x| x.1.0 == ev.1 && x.0.index() != pre_story_id).unwrap();
          story.1.0 = ev.0;
          }
        _ => {println!("Implement error handling!")}
      }
    }
  }
}

fn delete_story(
  mut commands: Commands,
  mut evr: EventReader<DeleteStoryEvent>,
  query: Query<(Entity, &Level, &Height, &Transform)>,
) {
  if evr.iter().len() <= 0 {
    return;
  }

  for ev in evr.iter() {
    // ground floor should not be deleted
    if ev.0 == 0 {
      continue;
    }

    // Check if story exists
    match query.iter().find(|x| x.1.0 == ev.0) {
      Some(story) => {
        commands.entity(story.0).despawn_recursive();
      }
      None => {
        println!("Story not found!");
        continue;
      }
    }
  }
}

fn rearrange_stories(
  mut query: Query<(Entity, &mut Level, & Height, &mut Transform)>
) {
  let highest_story = query.iter().find(| x| x.1.0 == query.iter().map(|(_,l,_,_)|l).max().unwrap().0).unwrap().1.0;
  let mut prev_story = (0, 0.0, 0.0); // level, transform and height of the previous story

  let mut story_iter = 0;
  let mut negator = 1;

  loop {
    // If previous story was the last overground story.. continue with underground stories
    if story_iter > highest_story {
      negator = -1;
      story_iter = 0;
      continue;
    }

    // Try to get next story
    match query.iter_mut().find(| x| x.1.0 == story_iter as i32) {
      Some(mut story) => {
        // if it's ground story.. skip
        if story_iter == 0 {
          prev_story = (story.1.0, story.3.translation.y, story.2.0);
          story_iter += 1 * negator;
          continue;
        }

        // Sets the (new) level and position of the story. Will change if the previous story got deleted
        story.1.0 = prev_story.0 + 1 * negator;
        story.3.translation.y = prev_story.1 + (prev_story.2 / 2.0 + story.2.0 / 2.0) * negator as f32;

        prev_story = (story.1.0, story.3.translation.y, story.2.0);
        story_iter += 1 * negator;
      }
      // Reaches None if story got deleted => contiues with next; or if there is no more story left
      None => {
        // Check if there is no more story
        if story_iter < query.iter().find(| x| x.1.0 == query.iter().map(|(_,l,_,_)|l).min().unwrap().0).unwrap().1.0 {
          break;
        }

        // If there are stories left to iterate through continue
        story_iter += 1 * negator;
      }
    }
  }
}

#[test]
fn did_ground_floor_spawn() {
  use bevy::render::mesh::MeshPlugin;

  // Setup app
  let mut app = App::new();

  // Loading Plugins for asset server
  app.add_plugin(AssetPlugin::default());
  app.add_plugin(MeshPlugin);
  app.add_plugin(MaterialPlugin::<StandardMaterial>::default());

  // Add Systems
  app.add_startup_system(add_ground_floor);

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 1);
  assert_eq!(app.world.query::<&Level>().iter(&app.world).nth(0).unwrap().0, 0);
  assert_eq!(app.world.query::<&Height>().iter(&app.world).nth(0).unwrap().0, 5.0);
  assert_eq!(app.world.query::<&Transform>().iter(&app.world).nth(0).unwrap().translation.y, 0.0);
}

#[test]
fn try_add_story_with_negative_height() {
  use bevy::render::mesh::MeshPlugin;

  // Setup app
  let mut app = App::new();

  // Loading Plugins for asset server
  app.add_plugin(AssetPlugin::default());
  app.add_plugin(MeshPlugin);
  app.add_plugin(MaterialPlugin::<StandardMaterial>::default());

  // Add Systems
  app.add_startup_system(add_ground_floor);
  app.add_system_to_stage(CoreStage::PreUpdate, add_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<AddStoryEvent>();

  // send events
  app.world.resource_mut::<Events<AddStoryEvent>>().send(AddStoryEvent(-2.0, StoryType::Overground));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 1);

  assert_eq!(app.world.query::<&Level>().iter(&app.world).nth(0).unwrap().0, 0);
  assert_eq!(app.world.query::<&Height>().iter(&app.world).nth(0).unwrap().0, 5.0);
  assert_eq!(app.world.query::<&Transform>().iter(&app.world).nth(0).unwrap().translation.y, 0.0);
}

#[test]
fn add_overground_stories() {
  use bevy::render::mesh::MeshPlugin;

  // Setup app
  let mut app = App::new();

  // Loading Plugins for asset server
  app.add_plugin(AssetPlugin::default());
  app.add_plugin(MeshPlugin);
  app.add_plugin(MaterialPlugin::<StandardMaterial>::default());

  // Add Systems
  app.add_startup_system(add_ground_floor);
  app.add_system_to_stage(CoreStage::PreUpdate, add_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<AddStoryEvent>();

  // send events
  app.world.resource_mut::<Events<AddStoryEvent>>().send(AddStoryEvent(2.0, StoryType::Overground));
  app.world.resource_mut::<Events<AddStoryEvent>>().send(AddStoryEvent(4.0, StoryType::Overground));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 3);

  assert_eq!(app.world.query::<&Level>().iter(&app.world).nth(0).unwrap().0, 0);
  assert_eq!(app.world.query::<&Height>().iter(&app.world).nth(0).unwrap().0, 5.0);
  assert_eq!(app.world.query::<&Transform>().iter(&app.world).nth(0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.query::<&Level>().iter(&app.world).nth(1).unwrap().0, 1);
  assert_eq!(app.world.query::<&Height>().iter(&app.world).nth(1).unwrap().0, 2.0);
  assert_eq!(app.world.query::<&Transform>().iter(&app.world).nth(1).unwrap().translation.y, 3.5);

  assert_eq!(app.world.query::<&Level>().iter(&app.world).nth(2).unwrap().0, 2);
  assert_eq!(app.world.query::<&Height>().iter(&app.world).nth(2).unwrap().0, 4.0);
  assert_eq!(app.world.query::<&Transform>().iter(&app.world).nth(2).unwrap().translation.y, 6.5);
}

#[test]
fn add_underground_stories() {
  use bevy::render::mesh::MeshPlugin;

  // Setup app
  let mut app = App::new();

  // Loading Plugins for asset server
  app.add_plugin(AssetPlugin::default());
  app.add_plugin(MeshPlugin);
  app.add_plugin(MaterialPlugin::<StandardMaterial>::default());

  // Add Systems
  app.add_startup_system(add_ground_floor);
  app.add_system_to_stage(CoreStage::PreUpdate, add_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<AddStoryEvent>();

  // send events
  app.world.resource_mut::<Events<AddStoryEvent>>().send(AddStoryEvent(2.0, StoryType::Underground));
  app.world.resource_mut::<Events<AddStoryEvent>>().send(AddStoryEvent(4.0, StoryType::Underground));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 3);

  assert_eq!(app.world.query::<&Level>().iter(&app.world).nth(0).unwrap().0, 0);
  assert_eq!(app.world.query::<&Height>().iter(&app.world).nth(0).unwrap().0, 5.0);
  assert_eq!(app.world.query::<&Transform>().iter(&app.world).nth(0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.query::<&Level>().iter(&app.world).nth(1).unwrap().0, -1);
  assert_eq!(app.world.query::<&Height>().iter(&app.world).nth(1).unwrap().0, 2.0);
  assert_eq!(app.world.query::<&Transform>().iter(&app.world).nth(1).unwrap().translation.y, -3.5);

  assert_eq!(app.world.query::<&Level>().iter(&app.world).nth(2).unwrap().0, -2);
  assert_eq!(app.world.query::<&Height>().iter(&app.world).nth(2).unwrap().0, 4.0);
  assert_eq!(app.world.query::<&Transform>().iter(&app.world).nth(2).unwrap().translation.y, -6.5);
}

#[test]
fn try_edit_not_existing_story() {
  use bevy::render::mesh::MeshPlugin; 

  // Setup app
  let mut app = App::new();

  // Loading Plugins for asset server
  app.add_plugin(AssetPlugin::default());
  app.add_plugin(MeshPlugin);
  app.add_plugin(MaterialPlugin::<StandardMaterial>::default());

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, edit_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<EditStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l1 = app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 3.5, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<EditStoryEvent>>().send(EditStoryEvent(2, 4.0));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 2);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 3.5);
}

#[test]
fn try_edit_height_with_negative_height() {
  use bevy::render::mesh::MeshPlugin; 

  // Setup app
  let mut app = App::new();

  // Loading Plugins for asset server
  app.add_plugin(AssetPlugin::default());
  app.add_plugin(MeshPlugin);
  app.add_plugin(MaterialPlugin::<StandardMaterial>::default());

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, edit_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<EditStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l1 = app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 3.5, 0.0))).id();
  let l2 = app.world.spawn((Level(2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 6.0, 0.0))).id();
  let l3 = app.world.spawn((Level(3), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 8.5, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<EditStoryEvent>>().send(EditStoryEvent(2, -4.0));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 4);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 3.5);

  assert_eq!(app.world.get::<Level>(l2).unwrap().0, 2);
  assert_eq!(app.world.get::<Height>(l2).unwrap().0, 3.0);
  assert_eq!(app.world.get::<Transform>(l2).unwrap().translation.y, 6.0);

  assert_eq!(app.world.get::<Level>(l3).unwrap().0, 3);
  assert_eq!(app.world.get::<Height>(l3).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l3).unwrap().translation.y, 8.5);
}

#[test]
fn edit_height_ground_story() {
  use bevy::render::mesh::MeshPlugin;

  // Setup app
  let mut app = App::new();

  // Loading Plugins for asset server
  app.add_plugin(AssetPlugin::default());
  app.add_plugin(MeshPlugin);
  app.add_plugin(MaterialPlugin::<StandardMaterial>::default());

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, edit_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<EditStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<EditStoryEvent>>().send(EditStoryEvent(0, 4.0));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 1);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 4.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);
}

#[test]
fn edit_height_one_overground_story() {
  use bevy::render::mesh::MeshPlugin; 

  // Setup app
  let mut app = App::new();

  // Loading Plugins for asset server
  app.add_plugin(AssetPlugin::default());
  app.add_plugin(MeshPlugin);
  app.add_plugin(MaterialPlugin::<StandardMaterial>::default());

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, edit_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<EditStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l1 = app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 3.5, 0.0))).id();
  let l2 = app.world.spawn((Level(2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 6.0, 0.0))).id();
  let l3 = app.world.spawn((Level(3), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 8.5, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<EditStoryEvent>>().send(EditStoryEvent(2, 4.0));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 4);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 3.5);

  assert_eq!(app.world.get::<Level>(l2).unwrap().0, 2);
  assert_eq!(app.world.get::<Height>(l2).unwrap().0, 4.0);
  assert_eq!(app.world.get::<Transform>(l2).unwrap().translation.y, 6.5);

  assert_eq!(app.world.get::<Level>(l3).unwrap().0, 3);
  assert_eq!(app.world.get::<Height>(l3).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l3).unwrap().translation.y, 9.5);
}

#[test]
fn edit_height_one_underground_story() {
  use bevy::render::mesh::MeshPlugin;

  // Setup app
  let mut app = App::new();

  // Loading Plugins for asset server
  app.add_plugin(AssetPlugin::default());
  app.add_plugin(MeshPlugin);
  app.add_plugin(MaterialPlugin::<StandardMaterial>::default());

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, edit_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<EditStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l1 = app.world.spawn((Level(-1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -3.5, 0.0))).id();
  let l2 = app.world.spawn((Level(-2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -6.0, 0.0))).id();
  let l3 = app.world.spawn((Level(-3), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -8.5, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<EditStoryEvent>>().send(EditStoryEvent(-2, 4.0));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 4);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, -1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, -3.5);

  assert_eq!(app.world.get::<Level>(l2).unwrap().0, -2);
  assert_eq!(app.world.get::<Height>(l2).unwrap().0, 4.0);
  assert_eq!(app.world.get::<Transform>(l2).unwrap().translation.y, -6.5);

  assert_eq!(app.world.get::<Level>(l3).unwrap().0, -3);
  assert_eq!(app.world.get::<Height>(l3).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l3).unwrap().translation.y, -9.5);
}

#[test]
fn edit_height_multiple_stories() {
  use bevy::render::mesh::MeshPlugin;

  // Setup app
  let mut app = App::new();

  // Loading Plugins for asset server
  app.add_plugin(AssetPlugin::default());
  app.add_plugin(MeshPlugin);
  app.add_plugin(MaterialPlugin::<StandardMaterial>::default());

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, edit_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<EditStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l1 = app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 3.5, 0.0))).id();
  let l2 = app.world.spawn((Level(2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 6.0, 0.0))).id();
  let l3 = app.world.spawn((Level(3), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 8.5, 0.0))).id();
  let l4 = app.world.spawn((Level(4), Height(6.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 12.5, 0.0))).id();
  let l5 = app.world.spawn((Level(5), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 16.5, 0.0))).id();

  let ln1 = app.world.spawn((Level(-1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -3.5, 0.0))).id();
  let ln2 = app.world.spawn((Level(-2), Height(6.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -7.5, 0.0))).id();
  let ln3 = app.world.spawn((Level(-3), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -12.0, 0.0))).id();
  let ln4 = app.world.spawn((Level(-4), Height(7.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -17.0, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<EditStoryEvent>>().send(EditStoryEvent(1, 3.0));
  app.world.resource_mut::<Events<EditStoryEvent>>().send(EditStoryEvent(2, 4.0));
  app.world.resource_mut::<Events<EditStoryEvent>>().send(EditStoryEvent(4, 5.0));

  app.world.resource_mut::<Events<EditStoryEvent>>().send(EditStoryEvent(-1, 4.0));
  app.world.resource_mut::<Events<EditStoryEvent>>().send(EditStoryEvent(-3, 1.0));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 10);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 3.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 4.0);

  assert_eq!(app.world.get::<Level>(l2).unwrap().0, 2);
  assert_eq!(app.world.get::<Height>(l2).unwrap().0, 4.0);
  assert_eq!(app.world.get::<Transform>(l2).unwrap().translation.y, 7.5);

  assert_eq!(app.world.get::<Level>(l3).unwrap().0, 3);
  assert_eq!(app.world.get::<Height>(l3).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l3).unwrap().translation.y, 10.5);

  assert_eq!(app.world.get::<Level>(l4).unwrap().0, 4);
  assert_eq!(app.world.get::<Height>(l4).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l4).unwrap().translation.y, 14.0);

  assert_eq!(app.world.get::<Level>(l5).unwrap().0, 5);
  assert_eq!(app.world.get::<Height>(l5).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l5).unwrap().translation.y, 17.5);

  assert_eq!(app.world.get::<Level>(ln1).unwrap().0, -1);
  assert_eq!(app.world.get::<Height>(ln1).unwrap().0, 4.0);
  assert_eq!(app.world.get::<Transform>(ln1).unwrap().translation.y, -4.5);

  assert_eq!(app.world.get::<Level>(ln2).unwrap().0, -2);
  assert_eq!(app.world.get::<Height>(ln2).unwrap().0, 6.0);
  assert_eq!(app.world.get::<Transform>(ln2).unwrap().translation.y, -9.5);

  assert_eq!(app.world.get::<Level>(ln3).unwrap().0, -3);
  assert_eq!(app.world.get::<Height>(ln3).unwrap().0, 1.0);
  assert_eq!(app.world.get::<Transform>(ln3).unwrap().translation.y, -13.0);

  assert_eq!(app.world.get::<Level>(ln4).unwrap().0, -4);
  assert_eq!(app.world.get::<Height>(ln4).unwrap().0, 7.0);
  assert_eq!(app.world.get::<Transform>(ln4).unwrap().translation.y, -17.0);
}

#[test]
fn try_swap_ground_story() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, swap_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<SwapStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l1 = app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 3.5, 0.0))).id();
  let l2 = app.world.spawn((Level(2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 6.0, 0.0))).id();
  let l3 = app.world.spawn((Level(3), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 8.5, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<SwapStoryEvent>>().send(SwapStoryEvent(0, 2));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 4);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 3.5);

  assert_eq!(app.world.get::<Level>(l2).unwrap().0, 2);
  assert_eq!(app.world.get::<Height>(l2).unwrap().0, 3.0);
  assert_eq!(app.world.get::<Transform>(l2).unwrap().translation.y, 6.0);

  assert_eq!(app.world.get::<Level>(l3).unwrap().0, 3);
  assert_eq!(app.world.get::<Height>(l3).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l3).unwrap().translation.y, 8.5);
}

#[test]
fn try_swap_same_story() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, swap_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<SwapStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l1 = app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 3.5, 0.0))).id();
  let l2 = app.world.spawn((Level(2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 6.0, 0.0))).id();
  let l3 = app.world.spawn((Level(3), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 8.5, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<SwapStoryEvent>>().send(SwapStoryEvent(1, 1));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 4);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 3.5);

  assert_eq!(app.world.get::<Level>(l2).unwrap().0, 2);
  assert_eq!(app.world.get::<Height>(l2).unwrap().0, 3.0);
  assert_eq!(app.world.get::<Transform>(l2).unwrap().translation.y, 6.0);

  assert_eq!(app.world.get::<Level>(l3).unwrap().0, 3);
  assert_eq!(app.world.get::<Height>(l3).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l3).unwrap().translation.y, 8.5);
}

#[test]
fn try_swap_not_existing_story1() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, swap_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<SwapStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<SwapStoryEvent>>().send(SwapStoryEvent(1, 2));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 1);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);
}

#[test]
fn try_swap_not_existing_story2() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, swap_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<SwapStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l1 = app.world.spawn((Level(3), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 10.0, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<SwapStoryEvent>>().send(SwapStoryEvent(1, 2));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 2);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 5.0);
}

#[test]
fn swap_one_overground_story() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, swap_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<SwapStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l3 = app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 3.5, 0.0))).id();
  let l2 = app.world.spawn((Level(2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 6.0, 0.0))).id();
  let l1 = app.world.spawn((Level(3), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 10.0, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<SwapStoryEvent>>().send(SwapStoryEvent(1, 3));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 4);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 5.0);

  assert_eq!(app.world.get::<Level>(l2).unwrap().0, 2);
  assert_eq!(app.world.get::<Height>(l2).unwrap().0, 3.0);
  assert_eq!(app.world.get::<Transform>(l2).unwrap().translation.y, 9.0);

  assert_eq!(app.world.get::<Level>(l3).unwrap().0, 3);
  assert_eq!(app.world.get::<Height>(l3).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l3).unwrap().translation.y, 11.5);
}

#[test]
fn swap_one_underground_stories() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, swap_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<SwapStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l3 = app.world.spawn((Level(-1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -3.5, 0.0))).id();
  let l2 = app.world.spawn((Level(-2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -6.0, 0.0))).id();
  let l1 = app.world.spawn((Level(-3), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -10.0, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<SwapStoryEvent>>().send(SwapStoryEvent(-1, -3));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 4);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, -1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, -5.0);

  assert_eq!(app.world.get::<Level>(l2).unwrap().0, -2);
  assert_eq!(app.world.get::<Height>(l2).unwrap().0, 3.0);
  assert_eq!(app.world.get::<Transform>(l2).unwrap().translation.y, -9.0);

  assert_eq!(app.world.get::<Level>(l3).unwrap().0, -3);
  assert_eq!(app.world.get::<Height>(l3).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l3).unwrap().translation.y, -11.5);
}

#[test]
fn swap_multiple_overground_stories() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, swap_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<SwapStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l3 = app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 3.5, 0.0))).id();
  let l1 = app.world.spawn((Level(2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 6.0, 0.0))).id();
  let l2 = app.world.spawn((Level(3), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 10.0, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<SwapStoryEvent>>().send(SwapStoryEvent(1, 3));
  app.world.resource_mut::<Events<SwapStoryEvent>>().send(SwapStoryEvent(2, 1));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 4);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 3.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 4.0);

  assert_eq!(app.world.get::<Level>(l2).unwrap().0, 2);
  assert_eq!(app.world.get::<Height>(l2).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l2).unwrap().translation.y, 8.0);

  assert_eq!(app.world.get::<Level>(l3).unwrap().0, 3);
  assert_eq!(app.world.get::<Height>(l3).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l3).unwrap().translation.y, 11.5);
}

#[test]
fn swap_multiple_underground_stories() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, swap_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<SwapStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l3 = app.world.spawn((Level(-1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -3.5, 0.0))).id();
  let l1 = app.world.spawn((Level(-2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -6.0, 0.0))).id();
  let l2 = app.world.spawn((Level(-3), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -10.0, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<SwapStoryEvent>>().send(SwapStoryEvent(-1, -3));
  app.world.resource_mut::<Events<SwapStoryEvent>>().send(SwapStoryEvent(-2, -1));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 4);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, -1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 3.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, -4.0);

  assert_eq!(app.world.get::<Level>(l2).unwrap().0, -2);
  assert_eq!(app.world.get::<Height>(l2).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l2).unwrap().translation.y, -8.0);

  assert_eq!(app.world.get::<Level>(l3).unwrap().0, -3);
  assert_eq!(app.world.get::<Height>(l3).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l3).unwrap().translation.y, -11.5);
}

#[test]
fn swap_across_stories() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, swap_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<SwapStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let ln2 = app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 3.5, 0.0))).id();
  let l2 = app.world.spawn((Level(2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 6.0, 0.0))).id();

  let ln1 = app.world.spawn((Level(-1), Height(4.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -4.5, 0.0))).id();
  let l1 = app.world.spawn((Level(-2), Height(1.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -7.0, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<SwapStoryEvent>>().send(SwapStoryEvent(1, -2));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 5);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 1.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 3.0);

  assert_eq!(app.world.get::<Level>(l2).unwrap().0, 2);
  assert_eq!(app.world.get::<Height>(l2).unwrap().0, 3.0);
  assert_eq!(app.world.get::<Transform>(l2).unwrap().translation.y, 5.0);

  assert_eq!(app.world.get::<Level>(ln1).unwrap().0, -1);
  assert_eq!(app.world.get::<Height>(ln1).unwrap().0, 4.0);
  assert_eq!(app.world.get::<Transform>(ln1).unwrap().translation.y, -4.5);

  assert_eq!(app.world.get::<Level>(ln2).unwrap().0, -2);
  assert_eq!(app.world.get::<Height>(ln2).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(ln2).unwrap().translation.y, -7.5);
}

#[test]
fn try_delete_ground_story() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system(delete_story);

  // add Events
  app.add_event::<DeleteStoryEvent>();

  // Setup test entities
  app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0)));
  app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 5.0, 0.0)));

  // send events
  app.world.resource_mut::<Events<DeleteStoryEvent>>().send(DeleteStoryEvent(0));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 2);
  assert_eq!(app.world.query::<&Level>().iter(&app.world).nth(0).unwrap().0, 0);
  assert_eq!(app.world.query::<&Height>().iter(&app.world).nth(0).unwrap().0, 5.0);
  assert_eq!(app.world.query::<&Transform>().iter(&app.world).nth(0).unwrap().translation.y, 0.0);
}

#[test]
fn try_delete_not_existing_story() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system(delete_story);

  // add Events
  app.add_event::<DeleteStoryEvent>();

  // Setup test entities
  app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0)));

  // send events
  app.world.resource_mut::<Events<DeleteStoryEvent>>().send(DeleteStoryEvent(1));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 1);

  assert_eq!(app.world.query::<&Level>().iter(&app.world).nth(0).unwrap().0, 0);
  assert_eq!(app.world.query::<&Height>().iter(&app.world).nth(0).unwrap().0, 5.0);
  assert_eq!(app.world.query::<&Transform>().iter(&app.world).nth(0).unwrap().translation.y, 0.0);
}

#[test]
fn delete_one_overground_story() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, delete_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<DeleteStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let d1 = app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 3.5, 0.0))).id();
  let l1 = app.world.spawn((Level(2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 6.0, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<DeleteStoryEvent>>().send(DeleteStoryEvent(1));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 2);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 3.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 4.0);

  assert!(app.world.get::<Level>(d1).is_none());
}

#[test]
fn delete_one_underground_story() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, delete_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<DeleteStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let l1 = app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 3.5, 0.0))).id();
  let dn1 = app.world.spawn((Level(-1), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -4.0, 0.0))).id();
  let ln1 = app.world.spawn((Level(-2), Height(4.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -7.5, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<DeleteStoryEvent>>().send(DeleteStoryEvent(-1));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 3);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 3.5);

  assert_eq!(app.world.get::<Level>(ln1).unwrap().0, -1);
  assert_eq!(app.world.get::<Height>(ln1).unwrap().0, 4.0);
  assert_eq!(app.world.get::<Transform>(ln1).unwrap().translation.y, -4.5);

  assert!(app.world.get::<Level>(dn1).is_none());
}

#[test]
fn delete_multiple_stories() {
  // Setup app
  let mut app = App::new();

  // Add Systems
  app.add_system_to_stage(CoreStage::PreUpdate, delete_story);
  app.add_system(rearrange_stories);

  // add Events
  app.add_event::<DeleteStoryEvent>();

  // Setup test entities
  let l0 = app.world.spawn((Level(0), Height(5.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 0.0, 0.0))).id();
  let d1 = app.world.spawn((Level(1), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 3.5, 0.0))).id();
  let l1 = app.world.spawn((Level(2), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 6.0, 0.0))).id();
  let d2 = app.world.spawn((Level(3), Height(4.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 9.5, 0.0))).id();
  let l2 = app.world.spawn((Level(4), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, 12.5, 0.0))).id();

  let dn1 = app.world.spawn((Level(-1), Height(3.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -4.0, 0.0))).id();
  let dn2 = app.world.spawn((Level(-2), Height(4.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -7.5, 0.0))).id();
  let ln1 = app.world.spawn((Level(-3), Height(2.0))).insert(TransformBundle::from(Transform::from_xyz(0.0, -10.5, 0.0))).id();

  // send events
  app.world.resource_mut::<Events<DeleteStoryEvent>>().send(DeleteStoryEvent(1));
  app.world.resource_mut::<Events<DeleteStoryEvent>>().send(DeleteStoryEvent(3));

  app.world.resource_mut::<Events<DeleteStoryEvent>>().send(DeleteStoryEvent(-2));
  app.world.resource_mut::<Events<DeleteStoryEvent>>().send(DeleteStoryEvent(-1));

  // Run systems
  app.update();

  // Check
  assert_eq!(app.world.query::<(&Level, &Height)>().iter(&app.world).len(), 4);

  assert_eq!(app.world.get::<Level>(l0).unwrap().0, 0);
  assert_eq!(app.world.get::<Height>(l0).unwrap().0, 5.0);
  assert_eq!(app.world.get::<Transform>(l0).unwrap().translation.y, 0.0);

  assert_eq!(app.world.get::<Level>(l1).unwrap().0, 1);
  assert_eq!(app.world.get::<Height>(l1).unwrap().0, 3.0);
  assert_eq!(app.world.get::<Transform>(l1).unwrap().translation.y, 4.0);

  assert_eq!(app.world.get::<Level>(l2).unwrap().0, 2);
  assert_eq!(app.world.get::<Height>(l2).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(l2).unwrap().translation.y, 6.5);

  assert_eq!(app.world.get::<Level>(ln1).unwrap().0, -1);
  assert_eq!(app.world.get::<Height>(ln1).unwrap().0, 2.0);
  assert_eq!(app.world.get::<Transform>(ln1).unwrap().translation.y, -3.5);

  assert!(app.world.get::<Level>(d1).is_none());
  assert!(app.world.get::<Level>(d2).is_none());

  assert!(app.world.get::<Level>(dn1).is_none());
  assert!(app.world.get::<Level>(dn2).is_none());
}
